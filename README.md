# ForensicsB3

Plot the graph of the hexadecimal characters of the sections of a binary (ELF or PE)

<p style="text-align: center;">
<img src="images/demo_plt.png" width=500 alt="Matplotlib example">
Matplotlib example
<img src="images/demo_plotly.png" width=500 alt="Plotly example">
Plotly example
<img src="images/demo_3d.png" width=500 alt="3D example">
3D example
</p>

## Usage

**usage:** binaryvisualizer.py _file_ [options]

Plot hexadecimal values of an ELF/PE binary


<dl>
<table>
  <tr align="center">
    <td colspan="2"><b>Positional arguments</b></td>
  </tr>
  <tr>
    <td><b>Argument</b></td>
    <td><b>Description</b></td>
  </tr>
  <tr>
    <td><i>file</i></td>
    <td>ELF/PE file to parse</td>
  </tr>
  <tr align="center">
    <td colspan="2"><b>Optional arguments</b></td>
  </tr>
  <tr>
    <td><b>Argument</b></td>
    <td><b>Description</b></td>
  </tr>
  <tr>
    <td>-h, --help</td>
    <td>Display this information</td>
  </tr>
  <tr>
    <td>-d NBDIGITS, --nbdigits NBDIGITS</td>
    <td>Choose the maximum number of characters to plot (default : full file)</td>
  </tr>
  <tr>
    <td>-s [SECTIONS [SECTIONS ...]], --section [SECTIONS [SECTIONS ...]]</td>
    <td>Choose the sections to be plotted (default : all sections)</td>
  </tr>
  <tr>
    <td>-ns [NOTSECTIONS [NOTSECTIONS ...]], --notsection [NOTSECTIONS [NOTSECTIONS ...]]</td>
    <td>Choose the sections not to be plotted</td>
  </tr>
  <tr>
    <td>-names, --section-names</td>
    <td>Prints all the the specified file's sections (and doesn't plot anything)</td>
  </tr>
  <tr>
    <td>-nl, --nolegends</td>
    <td>Hide the legends (section names) on the matplotlib graph (takes less time to plot)</td>
  </tr>
  <tr>
    <td>--plotly</td>
    <td>Use plotly instead of matplotlib.pyplot to plot the curves. Waaay slower, but nicer HTML result.</td>
  </tr>
  <tr>
    <td>-v, --verbose</td>
    <td>Toggle verbose mode</td>
  </tr>
  <tr>
    <td>-3D</td>
    <td> `Experimental` : plot in 3D. Forces plotly off.</td>
  </tr>
</table>
</dl>

### Prerequisites

This project uses [pyelftools](https://github.com/eliben/pyelftools) and [plotly](https://plot.ly/python/).

```
$ pip install pyelftools
$ pip install plotly
```

### Installing

Install the prerequisites, download the code and directly run it.
```
$ cd forensicsb3/source
$ python binaryvisualizer.py _file_ [options]
```
There are some binaries provided in the "testbinaries" folder that you can use.


## Built With

* [pyelftools](https://github.com/eliben/pyelftools) - To parse elf files
* [plotly](https://plot.ly/python/) - For nice HTML plots
* [hexdump](https://pypi.org/project/hexdump/#description) - Used and modified this code to parse PE files

## Contributing

As this is a school project, we are not taking any contributions

## Authors

* **Aloïs DECONINCK** - *Eurecom*
* **Alban RECLY** - *Eurecom* - [github](https://github.com/nablar)


## License

This project is not licensed

## Acknowledgments

* Hat tip to anyone whose code was used
* [Inspiration](https://www.reddit.com/r/dataisbeautiful/comments/aua1vf/oc_minimalist_first_million_digits_of_pi_each/), given by our teacher Davide Balzarotti
