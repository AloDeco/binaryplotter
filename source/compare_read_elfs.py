# from devtools.dwarf import DWARF
from readelf import ReadElf
import sys
import io
import timeit


with open('binary.so', 'rb') as bin:
    start = timeit.timeit()
    output = io.StringIO()
    ELF = ReadElf(bin, output)
    for section in ELF.elffile.iter_sections():
        if not section.is_null():
            print(section.name)
            ELF.display_hex_dump(section.name)

    end = timeit.timeit()
    print(output.getvalue())
    print("Time : {}".format(start-end))

