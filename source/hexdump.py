#!/usr/bin/env python
# -*- coding: latin-1 -*-

# <-- removing this magic comment breaks Python 3.4 on Windows

__version__ = '3.3'
__author__ = 'anatoly techtonik <techtonik@gmail.com>'  # original author, modified code a lot
__license__ = 'Public Domain'

import binascii  # binascii is required for Python 3
import sys

# --- constants
PY3K = sys.version_info >= (3, 0)

""" ORIGINAL FUNCTIONS """


# --- workaround against Python consistency issues
def normalize_py():
    if sys.platform == "win32":
        # set sys.stdout to binary mode on Windows
        import os, msvcrt
        msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)


# --- - chunking helpers
def chunks(seq, size):
    '''Generator that cuts sequence (bytes, memoryview, etc.)
     into chunks of given size. If `seq` length is not multiply
     of `size`, the lengh of the last chunk returned will be
     less than requested.

     >>> list( chunks([1,2,3,4,5,6,7], 3) )
     [[1, 2, 3], [4, 5, 6], [7]]
  '''
    d, m = divmod(len(seq), size)
    for i in range(d):
        yield seq[i * size:(i + 1) * size]
    if m:
        yield seq[d * size:]


def chunkread(f, size):
    '''Generator that reads from file like object. May return less
     data than requested on the last read.'''
    c = f.read(size)
    while len(c):
        yield c
        c = f.read(size)


def genchunks(mixed, size):
    '''Generator to chunk binary sequences or file like objects.
     The size of the last chunk returned may be less than
     requested.'''
    if hasattr(mixed, 'read'):
        return chunkread(mixed, size)
    else:
        return chunks(mixed, size)


def dump(binary, size=2, sep=' '):
    '''
  Convert binary data (bytes in Python 3 and str in
  Python 2) to hex string like '00 DE AD BE EF'.
  `size` argument specifies length of text chunks
  and `sep` sets chunk separator.
  '''
    hexstr = binascii.hexlify(binary)
    if PY3K:
        hexstr = hexstr.decode('ascii')
    return sep.join(chunks(hexstr.upper(), size))


""" MODIFIED/CUSTOM FUNCTIONS """


def dumpgen(data, verbose=False):
    generator = genchunks(data, 16)
    bytecounter = 0
    wholedump = ""
    nbrsections = 0
    offset = 0
    optheadersize = 0

    for addr, d in enumerate(generator):
        dumpstr = dump(d)
        wholedump += dumpstr
        bytecounter += 16
        if bytecounter == 64:
            # end of DOS header, beginning of DOS stub
            # get the offset of the PE header
            offsetsplit = dumpstr[-11:].split(' ')
            offset = int(offsetsplit[3] + offsetsplit[2] + offsetsplit[1] + offsetsplit[0], 16)
        elif offset and bytecounter == (offset - offset % 16) + 16:
            # end of PE header, beginning of file header
            # get the number of sections and size of optional header
            # 50 45 is the PE magic number
            if "50 45" not in dumpstr:
                print("Error : magic number (50 45) not found : not a PE File")
                sys.exit(-1)
            nbrsections = int(dumpstr[dumpstr.index("50 45") + 18:dumpstr.index("50 45") + 20], 16)
        elif offset and bytecounter == (offset - offset % 16) + 32:
            endian = wholedump[wholedump.index("50 45") + 59:wholedump.index("50 45") + 64].split(' ')
            optheadersize = int(endian[1] + endian[0], 16)

    return getsections(offset, nbrsections, optheadersize, wholedump, verbose)


def getsections(PEoffset, nbsections, optheadersize, wholedump, verbose=False):
    wholedump = wholedump.replace(' ', '').replace('\n', '')
    sections = {}
    for section in range(nbsections):
        # each section in the section table occupies 40 bytes. They all follow each other.
        header = wholedump[
                 PEoffset * 2 + 32 + optheadersize * 2 + 40 * 2 * section: PEoffset * 2 + 32 + optheadersize * 2 + 40 * 2 * (
                         section + 1)]
        sectname = bytearray.fromhex(header[16:32].replace('00', '')).decode()
        rawdataptr = int(littleendian(header[56:64]), 16)
        size = int(littleendian(header[32:40]), 16)
        if verbose:
            print(
                "Section {} at offset 0x{} ({}), size 0x{} ({})".format(sectname,
                                                                        littleendian(header[56:64]), rawdataptr,
                                                                        littleendian(header[32:40]), size))
        if rawdataptr != 0:
            sections[sectname] = wholedump[rawdataptr:rawdataptr + size]
            if verbose:
                print(sections[sectname][:100]+"...")
        else:  # if the offset is 0, it means the content of the section is generated randomly at launch time,
            # and thus not present in the PE
            sections[sectname] = ""
    return sections


def littleendian(input):
    return input[6:] + input[4:6] + input[2:4] + input[:2]


def hexdump(data, verbose=False):
    if PY3K and type(data) == str:
        raise TypeError('Abstract unicode data (expected bytes sequence)')

    sectiondumps = dumpgen(data, verbose)
    if verbose:
        for section, dump in sectiondumps.items():
            print("Section {} : \n {}...".format(section, dump[:100]))
    return sectiondumps


def printsectioninfo(data, sections, notsections, verbose=False):
    sectiondumps = dumpgen(data, verbose)
    if not verbose:
        for section, dump in sectiondumps.items():
            if section not in notsections and (section in sections or -1 in sections):
                print(section)
