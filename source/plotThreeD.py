import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

PLOT_LEGENDS = True
TITLE = 'ELF/PE visualizer'

"""Dictionary that gives for each hexadecimal character the values of the translations for the x, y and z directions"""
cospi4 = math.cos(math.pi / 4) # to avoid computing this cosine each time
normpi4 = math.sqrt(3 * cospi4 ** 2)  # used to normalise
coordDictionary = {
    "0": (0, 0, 1),
    "1": (cospi4 / normpi4, cospi4 / normpi4, cospi4 / normpi4),
    "2": (-cospi4 / normpi4, cospi4 / normpi4, cospi4 / normpi4),
    "3": (-cospi4 / normpi4, -cospi4 / normpi4, cospi4 / normpi4),
    "4": (cospi4 / normpi4, -cospi4 / normpi4, cospi4 / normpi4),
    "5": (math.cos(math.pi / 6), math.sin(math.pi / 6), 0),
    "6": (0, 1, 0),
    "7": (math.cos(5 * math.pi / 6), math.sin(5 * math.pi / 6), 0),
    "8": (math.cos(-5 * math.pi / 6), math.sin(-5 * math.pi / 6), 0),
    "9": (0, -1, 0),
    "A": (math.cos(-math.pi / 6), math.sin(-math.pi / 6), 0),
    "B": (cospi4 / normpi4, cospi4 / normpi4, -cospi4 / normpi4),
    "C": (-cospi4 / normpi4, cospi4 / normpi4, -cospi4 / normpi4),
    "D": (-cospi4 / normpi4, -cospi4 / normpi4, -cospi4 / normpi4),
    "E": (cospi4 / normpi4, -cospi4 / normpi4, -cospi4 / normpi4),
    "F": (0, 0, -1),
    "a": (math.cos(-math.pi / 6), math.sin(-math.pi / 6), 0),
    "b": (cospi4 / normpi4, cospi4 / normpi4, -cospi4 / normpi4),
    "c": (-cospi4 / normpi4, cospi4 / normpi4, -cospi4 / normpi4),
    "d": (-cospi4 / normpi4, -cospi4 / normpi4, -cospi4 / normpi4),
    "e": (cospi4 / normpi4, -cospi4 / normpi4, -cospi4 / normpi4),
    "f": (0, 0, -1)
}


def set_title(title):
    """Function that takes the name of the binary file that is visualized"""
    global TITLE
    TITLE = title


def legends(bool):
    """Function that sets the boolean that controls if the legends of the sections are shown or not"""
    global PLOT_LEGENDS
    PLOT_LEGENDS = bool


def init():
    global fig, ax
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    #Set axis labels
    ax.set_xlabel('3 -> A direction')
    ax.set_ylabel('9 -> 6 direction')
    ax.set_zlabel('F -> 0 direction')
    #Remove the tick labels
    plt.gca().xaxis.set_ticks([])
    plt.gca().yaxis.set_ticks([])
    plt.gca().zaxis.set_ticks([])


def coordinates(n, hexa):
    """Function that computes the coordinates of the n first hexadecimal characters of a string hexa"""
    x, y, z = 0, 0, 0
    x_tab, y_tab, z_tab = [x], [y], [z]
    n = np.min((n, len(hexa)))
    for k in range(n):
        xhexa, yhexa, zhexa = coordDictionary[hexa[k]]
        x += xhexa
        y += yhexa
        z += zhexa
        x_tab.append(x)
        y_tab.append(y)
        z_tab.append(z)
    return x_tab, y_tab, z_tab


def threeDRepMatplotlib(n, hexa, title):
    """Function that plots the visual representation of one section of a binary file in 3D, using Matplotlib"""
    global fig, ax
    x_tab, y_tab, z_tab = coordinates(n, hexa)
    ax.plot(x_tab, y_tab, z_tab, label=title)

def threeDRepMatplotlibShow():
    """Function that plots and shows the visual representation of all the sections of a binary file in 3D,
    using Matplotlib """
    if PLOT_LEGENDS:
        plt.legend(bbox_to_anchor=(0, 1), loc="upper left", bbox_transform=fig.transFigure)
    plt.title(TITLE)
    threeDLegend()  # show the legend of the directions
    plt.show()

def threeDLegend():
    """Function that creates the legend graph of the 16 directions"""
    global fig, ax, ax2
    ax2 = fig.add_subplot(4, 6, 24, projection='3d')
    # Compute the coordinates and plot them
    for hexa in coordDictionary:  # plot each point + it's index as text above
        x, y, z = coordDictionary[hexa]
        ax2.scatter(x, y, z, color='b', s=2)
        ax2.text(x, y, z, hexa, size=8)
        ax2.quiver(0, 0, 0, x, y, z, length=1, color='g', linewidth=0.2)
    # Show center of the sphere in red
    ax2.scatter(0, 0, 0, color='r', s=3)
    # Put the title
    plt.title("Legend", size=8)
    # Set axis labels for the legend
    ax2.set_xlabel('3 -> A direction', fontsize=7)
    ax2.set_ylabel('9 -> 6 direction', fontsize=7)
    ax2.set_zlabel('F -> 0 direction', fontsize=7)
    # Remove the ticks
    plt.gca().xaxis.set_ticks([])
    plt.gca().yaxis.set_ticks([])
    plt.gca().zaxis.set_ticks([])
