import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

cospi4 = math.cos(math.pi/4)
normpi4 = math.sqrt(3*cospi4**2)

coordDictionary = {
    "0": (0, 0, 1),
    "1": (cospi4/normpi4, cospi4/normpi4, cospi4/normpi4),
    "2": (-cospi4/normpi4, cospi4/normpi4, cospi4/normpi4),
    "3": (-cospi4/normpi4, -cospi4/normpi4, cospi4/normpi4),
    "4": (cospi4/normpi4, -cospi4/normpi4, cospi4/normpi4),
    "5": (math.cos(math.pi/6), math.sin(math.pi/6), 0),
    "6": (0, 1, 0),
    "7": (math.cos(5*math.pi/6), math.sin(5*math.pi/6), 0),
    "8": (math.cos(-5*math.pi/6), math.sin(-5*math.pi/6), 0),
    "9": (0, -1, 0),
    "A": (math.cos(-math.pi/6), math.sin(-math.pi/6), 0),
    "B": (cospi4/normpi4, cospi4/normpi4, -cospi4/normpi4),
    "C": (-cospi4/normpi4, cospi4/normpi4, -cospi4/normpi4),
    "D": (-cospi4/normpi4, -cospi4/normpi4, -cospi4/normpi4),
    "E": (cospi4/normpi4, -cospi4/normpi4, -cospi4/normpi4),
    "F": (0, 0, -1),
    }

for hexa in coordDictionary: #plot each point + it's index as text above
    x, y, z = coordDictionary[hexa]
    ax.scatter(x,y, z, color='b')
    ax.text(x,y, z, hexa, size=15)
    ax.quiver(0, 0, 0, x, y, z, length=1, color = 'g', linewidth=0.5)

#show center of the sphere in red
ax.scatter(0, 0, 0, color='r')

plt.axis('off')
plt.show()
