import argparse
from readelf import ReadElf
import sys
import io
import time
import plotTwoDMatplotlib
import plotTwoDPlotly
import re
import plotThreeD
import hexdump
import os


def printSectionNames(file, sections, notsections, verbose=False):
    ELF = ReadElf(file, "")  # load the elf file
    for section in ELF.elffile.iter_sections():
        if section.name not in notsections:
            if -1 in sections or section.name in sections:
                print(section.name)


def getHexDump(file, sections=[-1], notsections=[], verbose=False):
    ELF = ReadElf(file, None)  # load the elf file
    if verbose:
        print("Done reading elf file.")
    sections = getSectionsDump(ELF, sections, notsections, verbose=verbose)  # get a dict of the dump
    return sections


def getSectionsDump(ELF, sections, notsections, verbose=False):
    if verbose:
        print("== Generating elf file hex dump...")
    # returns a dict where key = section name and value = section hex dump (no spaces, no 0x, just hex)
    res = {}
    for section in ELF.elffile.iter_sections():
        if not (section.is_null() or section.name in notsections):
            # -1 means all sections, otherwise we look at what sections are asked
            if -1 in sections or section.name in sections:
                if verbose:
                    print("Generating plot data for section {}".format(section.name))
                dump = io.StringIO()
                ELF.output = dump
                ELF.display_hex_dump(section.name)
                res[section.name] = dump.getvalue()
            elif verbose:
                print("Section {} is ignored as specified in arguments".format(section.name))
        else:
            if verbose:
                if section.is_null():
                    print("Section {} is ignored because it's null".format(section.name))
                else:
                    print("Section {} is ignored as specified in arguments".format(section.name))
    return res


def plotsections(filesections, maxchars, sections=[-1], notsections=[], verbose=False, starttime=0):
    if verbose:
        print("== Generating plot data...")
    all = maxchars == -1  # this boolean is true if the number of chars to plot was not limited
    if GraphType == "Matplotlib":
        plotTwoDMatplotlib.twoDInitMatplotlib()  # to create the figure
    if GraphType == "Plotly":
        plotlyCurves = []  # table that will contain the curves of the different sections
        plotlyCurves.append(plotTwoDPlotly.twoDLegendPlotly())
    if GraphType == "3D":
        plotThreeD.init()  # to create the figure
    for section, dump in filesections.items():
        if section in notsections:
            if verbose:
                print("Section {} is ignored as specified in arguments".format(section))
            continue
        if section in sections or -1 in sections:
            if verbose:
                print("Plotting section {}".format(section))
            if all:
                maxchars = len(dump) + 1
            if GraphType == "Matplotlib":
                plotTwoDMatplotlib.twoDRepMatplotlib(maxchars, dump, section)
            elif GraphType == "Plotly":
                plotlyCurves.append(plotTwoDPlotly.twoDRepPlotly(maxchars, dump, section))
            elif GraphType == "3D":
                plotThreeD.threeDRepMatplotlib(maxchars, dump, section)
            maxchars -= len(dump)  # update the number of chars plotted
            if maxchars <= 0:
                if verbose:
                    print("Character limit reached, done plotting.")
                break  # we are done plotting because number of chars was specified
    if GraphType == "Matplotlib":
        if verbose:
            print("Generating plot...")
            print("Time elapsed : {}s".format(time.time() - starttime))  # synchronous method call -> print time now
        plotTwoDMatplotlib.twoDRepMatplotlibShow()
    elif GraphType == "Plotly":
        if verbose:
            print("== Creating plot...")
        plotTwoDPlotly.twoDRepPlotlyShow(plotlyCurves)
    elif GraphType == "3D":
        if verbose:
            print("== Creating 3D plot...")
        plotThreeD.threeDRepMatplotlibShow()


SCRIPT_DESCRIPTION = 'Plot hexadecimal values of an ELF/PE binary'


def main():
    ELF_extensions = ['', '.axf', '.bin', '.elf', '.o', '.prx', '.puff', '.ko', '.mod', '.so']
    PE_extensions = ['.cpl', '.exe', '.dll', '.ocx', '.sys', '.scr', '.drv', '.efi']

    # parse the command-line arguments and invoke ReadElf
    argparser = argparse.ArgumentParser(
        usage='%(prog)s <binary-file> [options]',
        description=SCRIPT_DESCRIPTION,
        add_help=False,
        prog='binaryvisualizer.py')
    argparser.add_argument('file',
                           nargs='?', default=None,
                           help='ELF file to parse')
    argparser.add_argument('-h', '--help',
                           action='store_true', dest='help',
                           help='Display this information')
    argparser.add_argument('-d', '--nbdigits',
                           action='store',
                           dest='nbdigits',
                           type=int,
                           default=-1,
                           help="Choose the maximum number of characters to plot (default : full file)")
    argparser.add_argument('-s', '--section',
                           action='store',
                           dest='sections',
                           nargs="*",
                           help="Choose the sections to be plotted (default : all sections)")
    argparser.add_argument('-ns', '--notsection',
                           action='store',
                           dest='notsections',
                           nargs="*",
                           help="Choose the sections not to be plotted")
    argparser.add_argument('-names', '--section-names',
                           action='store_true',
                           dest='onlynames',
                           help="Print all the the specified file's sections names (and doesn't plot anything)")
    argparser.add_argument('-nl', '--nolegends',
                           action='store_false',
                           dest='legends',
                           help='Don\'t show the legends on the graph (takes less time to plot)')
    argparser.add_argument('--plotly',
                           action='store_true',
                           dest='plotly',
                           help='Use plotly instead of matplotlib.pyplot to plot the curves. Waaay slower, but nicer '
                                'HTML result.')
    argparser.add_argument('-v', '--verbose',
                           action='store_true',
                           dest='verbose',
                           help='Toggle verbose mode')
    argparser.add_argument('-3D',
                           action='store_true',
                           dest='threeD',
                           help='Plot in 3 dimensions')

    args = argparser.parse_args()
    start = time.time()

    if args.help or not args.file:
        argparser.print_help(sys.stderr)
        sys.exit(0)

    # get the file extension to know if we are dealing with PE or ELF
    _, file_extension = os.path.splitext(args.file)

    sect = [-1]
    notsect = []
    if args.sections:
        sect = args.sections
    if args.notsections:
        notsect = args.notsections

    plotTwoDMatplotlib.legends(args.legends)  # tell if legends should be shown
    plotTwoDPlotly.legends(args.legends)  # tell if legends should be shown
    plotThreeD.legends(args.legends)  # tell if legends should be shown
    plotTwoDMatplotlib.set_title(args.file)  # set the graph file
    plotTwoDPlotly.set_title(args.file)
    plotThreeD.set_title(args.file)

    # set graph type
    global GraphType
    if args.plotly:
        GraphType = "Plotly"
    else:
        if args.threeD:
            GraphType = "3D"
        else:
            GraphType = "Matplotlib"

    if args.onlynames:
        with open(args.file, 'rb') as file:
            if file_extension in ELF_extensions:
                if args.verbose:
                    print("== Sections of ELF file {} :".format(args.file))
                printSectionNames(file, sect, notsect, args.verbose)

            elif file_extension in PE_extensions:
                if args.verbose:
                    print("== Sections of PE file {} :".format(args.file))
                hexdump.printsectioninfo(file.read(), sect, notsect, args.verbose)
        sys.exit(0)

    with open(args.file, 'rb') as file:
        sections = []
        if file_extension in ELF_extensions:
            if args.verbose:
                print("== Reading ELF file...")
            sections = getHexDump(file, sect, notsect, args.verbose)

        elif file_extension in PE_extensions:
            if args.verbose:
                print("== Reading PE file...")
            sections = hexdump.hexdump(file.read(), args.verbose)
        plotsections(sections, args.nbdigits, sect, notsect, args.verbose, starttime=start)

    end = time.time()
    if args.verbose and GraphType == "Plotly":
        print("Time elapsed : {}s".format(end - start))


if __name__ == '__main__':
    main()
